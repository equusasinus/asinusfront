import { Book } from './../models/book-model';
import { BookService } from './../services/book/book.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.scss']
})
export class BookComponent implements OnInit {
  formBook: FormGroup;
  
  constructor(private formBuilder: FormBuilder, private bookService: BookService,private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.formBook = this.formBuilder.group({
      name: [null, Validators.required],
      author: [null, [Validators.required]],
    });
  }

  insertBook(){
    if (this.formBook?.valid) {
      const book: Book = {
        id: '',
        name: this.formBook.value?.name,
        author: this.formBook.value?.author
      }

      let t = this.bookService.insertBook(book);
      console.log(t);
      this.snackBar.open('inserted','', {
        duration: 3000
      });
    }else{
      console.error("error");
    }
  }

}
