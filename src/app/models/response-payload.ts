export interface ResponsePayload{
    success: boolean;
    value: any;
}