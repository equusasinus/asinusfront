export interface UserRegister { 
    firstName: string;
    lastName: string;
    password: string;
    username: string;
    email: string;
    birthday: Date;
}