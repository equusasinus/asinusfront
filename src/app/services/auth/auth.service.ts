import { environment } from './../../../environments/environment';
import { Auth } from './../../models/auth-model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpClient: HttpClient) { }

  login(auth: Auth) {
    const url = environment.apiUrl + '/auth/login';
    this.httpClient.post(url, auth).subscribe(
      (result: any) => {
        localStorage.setItem('token', result.value);
      },
      error => {
        console.error(error);
      }
    )
  }

}
