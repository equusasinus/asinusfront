import { environment } from './../../../environments/environment';
import { Book } from './../../models/book-model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { stringify } from 'querystring';

@Injectable({
  providedIn: 'root'
})
export class BookService {
  
  constructor(private httpClient: HttpClient) { }

  insertBook(book: Book) {
    const url = environment.apiUrl + '/book/insert';
    this.httpClient.post(url, book).subscribe(
      (result: any) => {
        return result;
      },
      error => {
        return "Error occured";
      }
    )
  }

  insert(book: Book): any {
    let returnValue = "";
    const url = environment.apiUrl + '/book/insert';
    this.httpClient.post(url, book).subscribe(
      (result: any) => {
        returnValue=result;
      },
      error => {
        returnValue ="Error occured";
      }
    )
    return returnValue;
  }

}
