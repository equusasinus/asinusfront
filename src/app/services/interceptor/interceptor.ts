import {
    HttpInterceptor,
    HttpRequest,
    HttpHandler,
    HttpEvent,
} from "@angular/common/http"
import { Observable } from "rxjs"

export class Interceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token = localStorage.getItem('token');
        if (token) {
            const cloneReq = req.clone({
                setHeaders: {
                    Authorization: 'Bearer '.concat(token)
                }
            })
            return next.handle(cloneReq);
        } else {
            return next.handle(req);
        }
    }
 
}