import { environment } from './../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private httpClient: HttpClient) { }

  insertBook() {
    const url = environment.apiUrl + '/insertBook';
    this.httpClient.get(url).subscribe(
      (result: any) => {
        console.log(result);
      },
      error => {
        console.error(error);
      }
    )
  }
}
