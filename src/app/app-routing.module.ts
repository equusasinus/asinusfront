import { BookComponent } from './book/book.component';
import { LoginComponent } from './login/login.component';
import { AppComponent } from './app.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegistrationComponent } from './registration/registration.component';

const routes: Routes = [
  {
    path:'home',
    component: AppComponent
  },
  {
    path:'login',
    component: LoginComponent
  },
  {
    path:'book',
    component: BookComponent
  },
  {
    path:'register',
    component: RegistrationComponent
  }
  /* {path:'users', component:UsersComponent,children:[{path:':id/:name',component:UserComponent}]},
  {path:'servers', canActivateChild:[AuthGuard],component:ServersComponent,children:[{path:':id/edit',canDeactivate:[CanDeactivateGuard],component:EditServerComponent},
  {path:':id',component:ServerComponent}]},
  {path:'not-found',component:PageNotFoundComponent},
  {path:'**',redirectTo:'/not-found',pathMatch:'full'} */
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
